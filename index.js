// server.js

// set up ========================
var express  = require('express');
var app      = express();                               // create our app w/ express
var mongoose = require('mongoose');                     // mongoose for mongodb
var morgan = require('morgan');             // log requests to the console (express4)
var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
var multer  = require('multer');
/*var upload = multer({ dest: 'public/uploads/' });*/
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/uploads/')
  },
  filename: function (req, file, cb) {
    //cb(null, file.fieldname + '-' + Date.now())
    let extArray = file.mimetype.split("/");
    let extension = extArray[extArray.length - 1];
    cb(null, file.originalname + '-' + Date.now()+ '.' +extension)
  }
})

var upload = multer({ storage: storage })

// configuration =================

mongoose.connect('mongodb://localhost/blog');     // connect to mongoDB database on modulus.io

app.use(express.static(__dirname + '/public'));                 // set the static files location /public/img will be /img for users
app.use(morgan('dev'));                                         // log every request to the console
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());

//below line to make api corss access
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


// define model =================
    var posts = mongoose.model('posts', {
        title       : String,
        excerpt     : String,
        description : String,
        picUrl      : String,
        createdAt   : { type : Date, default: Date.now },
        done        : Boolean
    });
	
	// get all posts
    app.get('/api/posts', function(req, res) {

        // use mongoose to get all posts in the database
        posts.find(function(err, posts) {

            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                res.send(err)

            res.json(posts); // return all posts in JSON format
        });
    });

    app.post('/api/posts', upload.single('featuredPic'), function (req, res, next) {
        // req.file is the `avatar` file
        // req.body will hold the text fields, if there were any
        //res.send(req.file);
        // console.log(req.file);

        // create a post, information comes from AJAX request from Angular
        posts.create({
            title       : req.body.title,
            excerpt     : req.body.excerpt,
            description : req.body.description,
            picUrl      : 'uploads/'+ req.file.filename,
            done : false
        }, function(err, post) {
            if (err)
                res.send(err);

            // get and return all the todos after you create another
            posts.find(function(err, posts) {
                if (err)
                    res.send(err)
                res.json(posts);
            });
        });
    });

    //delete a post
    app.delete('/api/posts/:postId', function(req, res){
        posts.remove({
            _id: req.params.postId
        }, function(err, post){
            if(err)
                res.send(err)

            //get and return all posts after you remove one
            posts.find(function(err, posts) {
                if (err)
                    res.send(err)
                res.json(posts);
            });
        });
    });

    //get a single post data
    app.get('/api/post/:postId', function(req, res){
        posts.findOne({
            _id: req.params.postId
        }, function(err, post){
             if(err)
                res.send(err);
            else
                res.json(post);
        });
    });

    /*edit a post*/

    app.post('/api/post/:postId', function(req, res){
        const userUpdate = { title: req.body.title, 
                            excerpt : req.body.excerpt, 
                            description : req.body.description 
                        };
        posts.findOneAndUpdate(
            {_id: req.params.postId}, // find a document with that filter
            userUpdate, // document to insert 
            {upsert: true, new: true, runValidators: true}, // options
            function (err, post) { // callback
                if (err) console.log('ERROR '+ err);
                else //get and return all posts after you remove one
                posts.find(function(err, posts) {
                    if (err)
                        res.send(err)
                    res.json(posts);
                });

            }
        );
    });

	
	// application -------------------------------------------------------------
    app.get('*', function(req, res) {
        res.sendfile('./public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
    });

// listen (start app with node server.js) ======================================
app.listen(4040);
console.log("App listening on port 4040");