var app = angular.module('blog', []);

app.controller('mainController', function($scope,$http){
		$scope.formData = {};
		
	// when landing on the page, get all posts and show them
		$http.get('/api/posts')
        .success(function(data) {
            $scope.posts = data;
            console.log(data);
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });
		
	// when submitting the add form, send the text to the node API
    $scope.createPost = function() {
		//console.log($scope.formData);
        var fdata = new FormData();
        var title = $scope.formData.title;
        var excerpt = $scope.formData.excerpt;
        var description = $scope.formData.description;
        fdata.append( 'featuredPic', jQuery('#featuredPic')[0].files[0] );
        fdata.append( 'title', title); 
        fdata.append( 'excerpt', excerpt); 
        fdata.append( 'description', description);
        /*$http.post('/api/posts', fdata)
            .success(function(data) {
                $scope.formData = {}; // clear the form so our user is ready to enter another
                $scope.posts = data;
                console.log(data);
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });*/
        jQuery.ajax({
           type: "POST",                
           url: "http://localhost:4040/api/posts",
           processData: false,
           contentType: false,
           cache:false,
           data: fdata,
           success: function(data){
                $scope.formData = {}; // clear the form so our user is ready to enter another
                $scope.posts = data;
                console.log(data);
            }
        });
    };


    /*delete a post*/
    $scope.deletePost = function(obj){
        console.log(obj.target.attributes.postId.value);
        var postId = obj.target.attributes.postId.value;
        $http.delete('/api/posts/' + postId)
            .success(function(data){
                $scope.editformData = {}; // clear the form
                $scope.posts = data;
            })
            .error(function(data){
                console.log('Error: ' + data);
            });
        obj.preventDefault();
    };

    /*for edit a post get post data*/
    $scope.editPost = function(obj){
        console.log(obj.target.attributes.postId.value);
        var postId = obj.target.attributes.postId.value;
        $http.get('/api/post/'+postId)
            .success(function(data){
                console.log(data);
                $scope.editformData = data;
            })
            .error(function(data){
                console.log('Error' + data);
            });
        obj.preventDefault();   
    };

    /*modify a post*/
    $scope.savePost = function(obj){
        console.log(obj.target.attributes.postId.value);
        var postId = obj.target.attributes.postId.value; 
        $http.post('/api/post/'+postId, $scope.editformData)
            .success(function(data){
                console.log(data);
                $scope.editformData = {}; // clear the form
                $scope.posts = data;
            })
            .error(function(data){
                console.log('Error' + data);
            });
        obj.preventDefault();   
    };

    /*sort by functionalities*/
    $scope.sortByItem = '-createdAt';
    $scope.myOrderBy = '-createdAt';
    $scope.sortByUpdate = function() {
        console.log($scope.sortByItem);
        $scope.myOrderBy = $scope.sortByItem;
    }
	
});